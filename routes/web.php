<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','ReportesController@index')->name('/');
Route::get('test','Controller@index')->name('test/');
Route::get('detalle','Controller@detalle')->name('detalle');


Route::get('reporte-existencias-sucursal','ReportesController@reporteSucursal')->name('reporte-existencias-sucursal');
Route::get('reporte-existencias','ReportesController@reporteExistencias')->name('reporte-existencias');
Route::get('exportar-sucursal/{sucursal_name}','ReportesController@exportarSucursal')->name('exportar-sucursal');
