@extends('layouts.app')
@section('content')
    <div class="container">
        <h2>Existencia: {{ $sucursal_name }}</h2>
        <br>
        <div class="row">
        <div class="col-md-6">
            <a href="{{ route('exportar-sucursal',['sucursal_name'=>$sucursal_name]) }}" class="btn btn-success float-left">Exportar</a>

        </div>

        <div class="col-md-6">
            <a href="{{ route('/') }}" class="btn btn-danger float-right">Regresar</a>
        </div>
        </div>
        <br><br>
        <table class="table table-striped table-bordered" id="table-sucursal" class="display">
            <thead>
            <tr>
                <th>Código</th>
                <th>Nombre</th>
                <th>Existencia</th>
            </tr>
            </thead>
            <tbody>
            @foreach($articulos as $articulo)
            <tr>
                <td>{{ $articulo->Itemcode }}</td>
                <td>{{ $articulo->ItemName }}</td>
                <td>{{ $articulo->OnHand }}</td>
            </tr>
                @endforeach
            </tbody>
        </table>
        </div>


    </div>
    @endsection
