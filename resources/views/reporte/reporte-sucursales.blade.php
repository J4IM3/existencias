@extends('layouts.app')
@section('content')
    <div class="container">
        <h2>Reporte todas las sucursales</h2>
        <table class="table table-striped table-bordered" id="reportes-sucursales">
            <thead>
            <tr>
                <th>Código</th>
                <th>Nombre</th>
                <th>Matriz</th>
                <th>Mayoreo</th>
                <th>Central</th>
                <th>Puebla Cedis</th>
            </tr>
            </thead>
            <tbody>
            @foreach($articulos as $articulo)
            <tr>
                <td>{{ $articulo->Itemcode }}</td>
                <td>{{ $articulo->ItemName }}</td>
                <td>{{ $articulo->Matriz }}</td>
                <td>{{ $articulo->Central }}</td>
                <td>{{ $articulo->Puebla_Cedis }}</td>
                <td>{{ $articulo->Mayoreo_Libros}}</td>
                <td>{{ $articulo->Merced }}</td>
                <td>{{ $articulo->Merced }}</td>
                <td>{{ $articulo->Merced }}</td>
                <td>{{ $articulo->Merced }}</td>
                <td>{{ $articulo->Merced }}</td>
            </tr>
                @endforeach
            </tbody>
        </table>
        <div class="row">
            <div class="col-md-6">

            </div>
            <div class="col-md-6">

                <a href="{{ route('/') }}" class="btn btn-danger float-right">Regresar</a>
            </div>
        </div>
    </div>
    @endsection