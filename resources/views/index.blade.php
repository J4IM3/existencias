@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="card text-center">
            <div class="card-header" style="background-color: #4298DE;">
                Elegir reporte
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="card">
                            <div class="card-body">
                                <h5 class="card-title">Seleccionar sucursal</h5>
                                <form action="{{ route('reporte-existencias-sucursal') }}" method="get">
                                    <div class="form-row">

                                        <div class="col-md-12 ">
                                            <div class="col-md-12 input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text" id="basic-addon1"><i class="far fa-calendar-alt"></i></span>
                                                </div>
                                                <select name="sucursal_name" class="form-control" id="">
                                                    <option value="">Selecciona una sucursal</option>
                                                    @foreach($sucursales as $sucursal)
                                                        <option value="{{ $sucursal->nombre}}">{{ $sucursal->nombre }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-12 col-sm-12" >
                                            <button type="submit" class="btn btn-success ">Visualizar</button>
                                        </div>
                                    </div>

                                </form>
                            </div>
                            <div class="card-footer text-muted">

                            </div>
                        </div>
                    </div>

                    <div class="col-sm-6">
                        <div class="card">
                            <div class="card-body">
                                <h5 class="card-title">Reporte sucursales</h5>
                                <form action="{{ route('reporte-existencias') }}" method="get">
                                    <div class="form-row">
                                        <div class="col-md-12 ">
                                            <br><br>
                                        </div>
                                        <div class="col-md-12 col-sm-12" >
                                            <button type="submit" class="btn btn-success ">Exportar</button>
                                        </div>
                                    </div>

                                </form>
                            </div>
                            <div class="card-footer text-muted">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <p>La ultima sincronización fue :  {{ $fechaActualizacion->actualizacion }}</p>
        </div>

    </div>
    @endsection
