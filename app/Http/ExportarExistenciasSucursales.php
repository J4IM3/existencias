<?php
/**
 * Created by PhpStorm.
 * User: ti
 * Date: 2020-03-25
 * Time: 19:06
 */

namespace App\Http;
use App\Http\Entities\Existencias;
use App\Http\Entities\Sucursales;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Illuminate\Support\Facades\DB;


class ExportarExistenciasSucursales implements FromCollection, WithMapping, WithHeadings
{
    public function collection()
    {
        $articulos = DB::select("select ex.`Itemcode`,ex.ItemName,
            sum(CASE WHEN ex.sucursal='Matriz'  THEN ex.OnHand ELSE null END )as Matriz,
            sum(CASE WHEN ex.sucursal='Mayoreo(Autoservicio)'  THEN ex.OnHand ELSE null END )as Mayoreo,
            sum(CASE WHEN ex.sucursal='Central'  THEN ex.OnHand ELSE null END )as Central,
            sum(CASE WHEN ex.sucursal='Puebla Cedis'  THEN ex.OnHand ELSE null END )as 'Puebla_Cedis',
            sum(CASE WHEN ex.sucursal='Mayoreo Libros'  THEN ex.OnHand ELSE null END )as 'Mayoreo_Libros',
            sum(CASE WHEN ex.sucursal='Merced'  THEN ex.OnHand ELSE null END )as Merced,
            sum(CASE WHEN ex.sucursal='Soledad'  THEN ex.OnHand ELSE null END )as Soledad,
            sum(CASE WHEN ex.sucursal='Sta Rosa'  THEN ex.OnHand ELSE null END )as 'Sta_Rosa',
            sum(CASE WHEN ex.sucursal='Xoxo'  THEN ex.OnHand ELSE null END )as Xoxo,
            sum(CASE WHEN ex.sucursal='Telemarketing'  THEN ex.OnHand ELSE null END )as Telemarketing,
            sum(CASE WHEN ex.sucursal='Central 2'  THEN ex.OnHand ELSE null END )as 'Central_2',
            sum(CASE WHEN ex.sucursal='Simbolos'  THEN ex.OnHand ELSE null END )as Simbolos,
            sum(CASE WHEN ex.sucursal='Reforma'  THEN ex.OnHand ELSE null END )as Reforma,
            sum(CASE WHEN ex.sucursal='Xalapa'  THEN ex.OnHand ELSE null END )as Xalapa,
            sum(CASE WHEN ex.sucursal='Alcala'  THEN ex.OnHand ELSE null END )as Alcala,
            sum(CASE WHEN ex.sucursal='Puebla 8'  THEN ex.OnHand ELSE null END )as 'Puebla_8'
            from existencias ex
            group by ex.Itemcode,ex.ItemName
        ");
        return $collection = Collection::make($articulos);
    }

    /**
     * @return array
     */
    public function headings(): array
    {
        return [
            'Código',
            'Articulo',
            'Matriz',
            'Mayoreo',
            'Central',
            'Puebla cedis',
            'Mayoreo libros',
            'Merced',
            'Soledad',
            'Santa Rosa',
            'Xoxo',
            'Telemarketing',
            'Central 2',
            'Simbolos',
            'Reforma',
            'Xalapa',
            'Alcala',
            'Puebla 8',
        ] ;
    }

    /**
     * @param mixed $row
     *
     * @return array
     */
    public function map($collection): array
    {
        return [
            $collection->Itemcode,
            $collection->ItemName,
            $collection->Matriz,
            $collection->Mayoreo,
            $collection->Central,
            $collection->Puebla_Cedis,
            $collection->Mayoreo_Libros,
            $collection->Merced,
            $collection->Soledad,
            $collection->Sta_Rosa,
            $collection->Xoxo,
            $collection->Telemarketing,
            $collection->Central_2,
            $collection->Simbolos,
            $collection->Reforma,
            $collection->Xalapa,
            $collection->Alcala,
            $collection->Puebla_8,
        ] ;
    }
}
