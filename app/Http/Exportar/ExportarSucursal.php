<?php
/**
 * Created by PhpStorm.
 * User: ti
 * Date: 2020-03-26
 * Time: 01:48
 */

namespace App\Http\Exportar;

use App\Http\Entities\Existencias;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Illuminate\Support\Facades\DB;

class ExportarSucursal implements FromCollection, WithMapping, WithHeadings
{
    protected $sucursal_name;

    function __construct($sucursal_name)
    {

        $this->sucursal_name = $sucursal_name;
    }

    /**
     * @return Collection
     */
    public function collection()
    {
        return Existencias::where('sucursal',$this->sucursal_name)->get();
    }

    /**
     * @return array
     */
    public function headings(): array
    {
        return [
            'Código',
            'Articulo',
            'Existencia'
        ] ;
    }

    /**
     * @param mixed $row
     *
     * @return array
     */
    public function map($existencias): array
    {
        return [
            $existencias->Itemcode,
            $existencias->ItemName,
            $existencias->OnHand,

        ] ;
    }
}