<?php

namespace App\Http\Controllers;

use App\CartVentasEscuelas;
use App\DetalleVentasEscuelas;
use App\Exports\VentasEscuelasExport;
use App\Http\ExportarExistenciasSucursales;
use App\Packages;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Maatwebsite\Excel\Facades\Excel;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    public function index()
    {
/*        $carts = CartVentasEscuelas::with('usersVe','adddressVE')
            ->get();

        foreach ($carts as $cart) {
            $cart->fullName = $cart->usersVe->name. " " . $cart->usersVe->apellidos;
            $cart->nivelAndGrade = $this->getNivelAndGrade($cart);
            $cart->paquetes = $this->getPackage($cart->package_id);
        }*/
        return Excel::download(new VentasEscuelasExport(),'ventas_escuelas.xlsx');
    }

    private function getPackage($packageId)
    {
        $arrayPackage = explode(',',$packageId);
        foreach ($arrayPackage as $value) {
            $package = Packages::select('descripcion','type')->where('id',$value)->first();
            $packageName[] = $package->descripcion. " - ". ($this->getType($package->type)) ;
        }
        return implode(', ',$packageName);
    }

    private function getType($type)
    {
        if ($type == "L") {
            return "Libreria";
        }
        if ($type == "P") {
            return "Papeleria";
        }
        return "Articulos optativos";
    }

    private function getNivelAndGrade($cart)
    {
        $nivel = $cart->usersVe->escuelaDetalle->level->descripcion;
        $grado = $cart->usersVe->escuelaDetalle->grade->descripcion;
        return $grado ." ". $nivel;
    }

    public function detalle()
    {
        $detalle = DetalleVentasEscuelas::all();
        foreach ($detalle as $value) {
            dd($value);
        }
    }




}
