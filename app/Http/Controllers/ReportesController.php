<?php

namespace App\Http\Controllers;

use App\Http\Entities\Existencias;
use App\Http\Entities\Precios;
use App\Http\Entities\Sucursales;
use App\Http\Exportar\ExportarSucursal;
use App\Http\ExportarExistenciasSucursales;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Input;
use Maatwebsite\Excel\Facades\Excel;

class ReportesController extends Controller
{
    public function index()
    {
        $sucursales = Sucursales::all();
        $fechaActualizacion = Precios::select('actualizacion')->where('actualizacion','<>',null)->first();
        return view('index',compact('sucursales','fechaActualizacion'));
    }
    public function reporteSucursal(Request $request)
    {
        $sucursal_name = $request->get('sucursal_name');
        $articulos =Existencias::where('sucursal',$sucursal_name)->get();
        return view('reporte.reporte-sucursal',compact('articulos','sucursal_name'));
    }

    public function reporteExistencias()
    {
ini_set('memory_limit', '2048M');

        return Excel::download(new ExportarExistenciasSucursales(),'reportes-sucursales.xlsx');
    }

    public function exportarSucursal($sucursal)
    {
        return Excel::download(new ExportarSucursal($sucursal),'existencias-sucursal-'.$sucursal.'.xlsx');
    }
}
