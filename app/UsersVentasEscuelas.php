<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UsersVentasEscuelas extends Model
{
    protected $connection = "laproveedora";
    protected $table = "users";

    public function usersVe()
    {
        return $this
            ->belongsTo(CartVentasEscuelas::class);
    }

    public function escuelaDetalle()
    {
        return $this
            ->hasOne(UsersDetail::class,'user_id','id');
    }
}
