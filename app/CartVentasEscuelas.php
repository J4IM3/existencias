<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CartVentasEscuelas extends Model
{
    protected $connection = "laproveedora";
    protected $table = "cart_ventas_escuelas";

    public function usersVe()
    {
        return $this
            ->hasOne(UsersVentasEscuelas::class,'id','user_id');
    }

    public function packages()
    {
        return $this
            ->hasOne(Packages::class,'id','package_id');
    }

    public function adddressVE()
    {
        return  $this
            ->hasOne(Address::class,'client_id','user_id');
    }

}
