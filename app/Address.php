<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    protected $connection = "laproveedora";
    protected $table = "addresses";

    public function adddressVE()
    {
        return $this
            ->belongsTo(CartVentasEscuelas::class);
    }
}
