<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Grades extends Model
{
    protected $connection = "laproveedora";
    protected $table = "study_grades";
}
