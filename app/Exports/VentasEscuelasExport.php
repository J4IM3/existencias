<?php

namespace App\Exports;

use App\CartVentasEscuelas;
use App\DetalleVentasEscuelas;
use App\Packages;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class VentasEscuelasExport implements FromCollection,WithMapping,WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $carts = CartVentasEscuelas::with('usersVe','adddressVE')
            ->get();

        foreach ($carts as $cart) {
            $cart->fullName = $cart->usersVe->name. " " . $cart->usersVe->apellidos;
            $cart->nivelAndGrade = $this->getNivelAndGrade($cart);
            $cart->paquetes = $this->getPackage($cart->package_id);
            $x = empty($cart->adddressVE) ? "" : " Referencia: ".$cart->adddressVE->referencia ;
            $cart->direccion = $cart->address_text." ". $x ;
        }

        return $carts;

    }

    public function headings(): array
    {
        return [
            'Cliente',
            'Nivel y grado',
            'Orden TL',
            'Paquetes agregados',
            'Fecha',
            'Metodo de pago',
            'Estado del pedido',
            'Direccion entrega',
            'Total',
        ];
    }


    public function map($row): array
    {
        return [
            $row->fullName,
            $row->nivelAndGrade,
            $row->order_id,
            $row->paquetes,
            $row->date,
            $row->payment_method,
            $row->status,
            $row->direccion,
            $row->total_total,
        ];
    }


    private function getPackage($packageId)
    {
        $arrayPackage = explode(',',$packageId);
        foreach ($arrayPackage as $value) {
            $package = Packages::select('descripcion','type')->where('id',$value)->first();
            $packageName[] = $package->descripcion. " - ". ($this->getType($package->type)) ;
        }
        return implode(', ',$packageName);
    }

    private function getType($type)
    {
        if ($type == "L") {
            return "Libreria";
        }
        if ($type == "P") {
            return "Papeleria";
        }
        return "Articulos optativos";
    }

    private function getNivelAndGrade($cart)
    {
        $nivel = $cart->usersVe->escuelaDetalle->level->descripcion;
        $grado = $cart->usersVe->escuelaDetalle->grade->descripcion;
        return $grado ." ". $nivel;
    }
}
