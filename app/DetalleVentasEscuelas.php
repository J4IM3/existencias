<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetalleVentasEscuelas extends Model
{
    protected $connection = "laproveedora";
    protected $table = "cart_detail_ventas_escuelas";
}
