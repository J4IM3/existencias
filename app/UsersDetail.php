<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UsersDetail extends Model
{
    protected $connection = "laproveedora";
    protected $table = "details_users_schools";

    public function escuelaDetalle()
    {
        return  $this
            ->belongsTo(UsersVentasEscuelas::class);
    }

    public function level()
    {
        return $this
            ->hasOne(Levels::class,'id','level_id');
    }

    public function grade()
    {
        return $this
            ->hasOne(Grades::class,'id','study_grade_id');
    }
}
