<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Packages extends Model
{
    protected $connection = "laproveedora";
    protected $table = "packages";

    public function packages()
    {
        return $this
            ->belongsTo(CartVentasEscuelas::class);
    }

}
